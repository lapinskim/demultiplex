#!/usr/bin/python3

import gzip
import sys
import argparse
from os import path
import operator
import subprocess

desc = '''
Demultiplex the reads from Undetermined FASTQ file using Illumina sequence
identifiers, Casava 1.8+ format.
'''

# TODO check proper FASTQ format;
# process 2 barcodes
# multiprocessing
# process pairend reads


def get_read(input_fq, pf, ns, index1_list, index2_list=False):
    line_index = 0
    read_number = 0
    print_line = False
    report = {}
    file_dict = {}

    handle = input_fq
    if input_fq.name.endswith('.gz'):
        proc = subprocess.Popen(['zcat', input_fq.name], bufsize=-1,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        handle = proc.stdout

    for line in handle:
        if line_index % 4 == 0:
            read_number += 1
            barcodes = line.decode("utf-8").strip("\n").rsplit(
                " ", maxsplit=1)[1].rsplit(
                    ":", maxsplit=1)[1].split("+", maxsplit=1)
            demultiplex_res = check_barcodes(barcodes, index1_list,
                                             index2_list)
            if demultiplex_res:
                bc = demultiplex_res[0]
                if bc not in report:
                    report[bc] = [0, 0]
                if demultiplex_res[1] == "p":
                    report[bc][0] += 1
                    print_line = True
                elif demultiplex_res[1] == "m":
                    report[bc][1] += 1
                    print_line = True
            else:
                print_line = False
        if print_line:
            if bc not in file_dict:
                file_dict[bc] = gzip.open(path.join(pf,
                                                    bc + '_' + ns + '.fq.gz'),
                                          'w')
            file_dict[bc].write(line)

        line_index += 1

    sys.stdout.write("Reads processed: {}\n".format(read_number))
    for barcode in report:
        sys.stdout.write('{}: perfect: {} ({}%); 1 missmatch: \
{} ({}%)\n'.format(barcode,
                   report[barcode][0],
                   report[barcode][0] * 100 / (report[barcode][0] +
                                               report[barcode][1]),
                   report[barcode][1],
                   report[barcode][1] * 100 / (report[barcode][0] +
                                               report[barcode][1])))


def check_barcodes(index_list, i1, i2=False):
    if len(index_list) == 1 and i2 is not False:
        sys.stderr.write("Only 1 barcode present, but 2 given.\n")
        sys.exit(0)
    if len(i1[0]) > len(index_list[0]):
        sys.stderr.write("Given barcode is longer than sequenced.\n")
        sys.exit(0)
    for list_entry in i1:
        distance = hamming_distance(index_list[0][:len(list_entry)],
                                    list_entry)
        if distance == 0:
            return(list_entry, "p")
        elif distance == 1:
            return(list_entry, "m")
    return


def hamming_distance(str1, str2):
    assert len(str1) == len(str2)
    return sum(map(operator.ne, str1, str2))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-i1', '--index1',
                        help='index 1 barcodes, space delimited',
                        type=str,
                        required=True,
                        nargs='+')
    parser.add_argument('-i2', '--index2',
                        help='index 2 barcodes, space delimited',
                        type=str,
                        nargs='+')
    parser.add_argument('-p', '--prefix',
                        help='output files prefix',
                        type=str,
                        default='./')
    parser.add_argument('-s', '--name_sufix',
                        help='filename sufix',
                        type=str,
                        default='R1')
    parser.add_argument('-f', '--file',
                        help='input FASTQ file',
                        required=True,
                        type=argparse.FileType('r'))
    args = parser.parse_args()

    i1_proc = [e.strip(',;').upper() for e in args.index1]
    i2_proc = False
    if args.index2:
        i2_proc = [e.strip(',;').upper() for e in args.index2]

    get_read(args.file, args.prefix, args.name_sufix, i1_proc, i2_proc)
