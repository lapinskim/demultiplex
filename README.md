# demultiplex
Demultiplex the reads from Undetermined FASTQ file using Illumina sequence identifiers, Casava 1.8+ format.

# Example
Demultiplex pair-end reads using two indexes:

```bash
./demultiplex.py \
  -i1 TAAGGC CGAGGC \
  -p ./reads \
  -s R1 \
  -f ./output/Undetermined_S0_R1_001.fastq.gz && \
  ./demultiplex.py \
  -i1 TAAGGC CGAGGC \
  -p ./reads \
  -s R2 \
  -f ./output/Undetermined_S0_R2_001.fastq.gz
  ```
